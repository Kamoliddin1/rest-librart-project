from django.contrib.auth.models import User
from rest_framework import serializers


class RegistrationSerializer(serializers.ModelSerializer):
    password2 = serializers.CharField(style={"input_type": "password"}, write_only=True)

    class Meta:
        model = User
        fields = ['password', 'password2', 'username', 'email']
        extra_kwargs = {
            'password': {'write_only': True}
        }

    def save(self):
        password = self.validated_data.get('password', None)
        password2 = self.validated_data.get('password2', None)
        email = User.objects.filter(email=self.validated_data['email'])
        if password != password2:
            raise serializers.ValidationError({'error': 'password are not the same'})
        #
        if email.exists():
            raise serializers.ValidationError({'error': 'email is already taken'})

        account = User(email=self.validated_data['email'], username=self.validated_data['username'])
        account.set_password(password)
        account.save()

        return account
