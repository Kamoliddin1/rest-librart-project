from rest_framework import status
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response
from rest_framework.views import APIView
from .serializers import RegistrationSerializer
from ..models import *


class LogoutAPIView(APIView):

    def post(self, request):
        if request.user.auth_token:
            request.user.auth_token.delete()
            return Response(status=status.HTTP_200_OK, data={'Successfully logged out'})
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=ValidationError({'Error': 'please provide token'}))


class RegisterAPIView(APIView):

    def post(self, request):
        serializer = RegistrationSerializer(data=request.data)
        data = {}
        if serializer.is_valid():
            account = serializer.save()
            data['username'] = account.username
            data['email'] = account.email
            token = Token.objects.get(user=account).key
            data['token'] = token

            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
