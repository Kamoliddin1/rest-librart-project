from rest_framework import serializers

from ..models import WatchList, StreamPlatform, Review


class ReviewSerializer(serializers.ModelSerializer):
    user = serializers.StringRelatedField(read_only=True)

    class Meta:
        model = Review
        exclude = ['watchlist', ]


class WatchListSerializer(serializers.ModelSerializer):
    stream = serializers.CharField(source="stream.name")
    reviews = ReviewSerializer(many=True, read_only=True)

    class Meta:
        model = WatchList
        fields = ['id',
                  'title',
                  'description',
                  'stream',
                  'active',
                  'created',
                  'reviews']


class StreamPlatformSerializer(serializers.HyperlinkedModelSerializer):
    watch_lists = WatchListSerializer(many=True, read_only=True)

    # watch_lists = serializers.StringRelatedField(many=True)
    # watch_lists = serializers.HyperlinkedRelatedField(
    #     many=True,
    #     read_only=True,
    #     view_name='movie-detail'
    # )

    class Meta:
        model = StreamPlatform
        fields = ["id",
                  "name",
                  "about",
                  "website",
                  "watch_lists"]

# class MovieSerializer(serializers.Serializer):
#     id = serializers.IntegerField(read_only=True)
#     name = serializers.CharField(validators=[name])
#     description = serializers.CharField()
#     active = serializers.BooleanField()
#
#     def create(self, validated_data):
#         return Movie.objects.create(**validated_data)
#
#     def update(self, instance, validated_data):
#         print('update keldi')
#
#         instance.name = validated_data.get('name', instance.name)
#         instance.description = validated_data.get('description', instance.description)
#         instance.active = validated_data.get('active', instance.active)
#         instance.save()
#         print('saved')
#         return instance
#
#     def validate_name(self, value):
#         if len(value) < 2:
#             print('validated kirdi')
#             raise serializers.ValidationError({"Error": "Please name is toooo short!"})
#         return value
