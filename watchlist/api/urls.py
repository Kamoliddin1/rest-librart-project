from django.urls import path
from .views import (
    WatchListAPIView,
    WatchDetailAPIView,
    PlatformListCreateAPIView,
    PlatformRetrieveUpdateDestroyAPIView,
    ReviewListAPIView,
    ReviewRetrieveAPIView, ReviewListCreateAPIView, UserReviewListAPIView,
)

urlpatterns = [
    path('list/', WatchListAPIView.as_view()),
    path('list/<int:pk>/', WatchDetailAPIView.as_view(), name='movie-detail'),
    path('stream/', PlatformListCreateAPIView.as_view()),
    path('stream/<int:pk>/', PlatformRetrieveUpdateDestroyAPIView.as_view(), name='stream-detail'),

    path('stream/<int:pk>/review/', ReviewListAPIView.as_view(), name='review-retrieve'),
    path('stream/<int:pk>/review-create/', ReviewListCreateAPIView.as_view(), name='review-create'),
    path('stream/review/<int:pk>/', ReviewRetrieveAPIView.as_view(), name='review-detail'),

    path('review/', UserReviewListAPIView.as_view(), name='usernames-review')
]
